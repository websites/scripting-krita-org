import json
import krita

def getChildrenInfo(target):
    dataToExport = []   
   
    for child in target:
        # Create object to convert to JSON later
        item = {
            "id": child.objectName(),
            "description": child.text().replace('&', ''),     
            "tooltip": child.toolTip(),
        }
        
        # Append to the list
        dataToExport.append(item)
    
    # Pretty print JSON
    json_data = json.dumps(dataToExport, indent=2)
    print(json_data)

getChildrenInfo(Krita.instance().actions())
