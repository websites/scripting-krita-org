#!/usr/bin/env python3

# the above line is required to run it on your computer. You will get security errors if you don't have it
import os
import sys
import glob # ability to find image files
from shutil import copyfile# ability to copy files
import json # final data format we wnat


kritaSourceCodeLocation = "/home/scott/git/krita/src/" # Where your Krita GIT repo SRC direcotry is at
kritaScriptingRepoLocation = "/home/scott/git/krita-scripting-pro-website/" # where this repository is at
iconDictionaryLocation = kritaScriptingRepoLocation + "public/assets/icon-dictionary/" # where to store all the icons 



# go through all the folders that contain Krita icons and copy them all to the same directory so we can load them later
# these locations were just grabbed by looking for all QRC file locations. QRC files are how Krita stores icons to use later
# https://invent.kde.org/graphics/krita/-/blob/master/krita/CMakeLists.txt#L50
iconLocations = [
	"krita/pics/Breeze-dark",
	"krita/pics/Breeze-light",
	"krita/pics/layerbox",
	"krita/pics/layerbox/svg",
	"krita/pics/layers/",
	"krita/pics/misc-light/",
	"krita/pics/misc-dark/",
	"krita/pics/paintops/",
	"krita/pics/tools/SVG/16/",
	"krita/pics/tool_transform/",
	"krita/pics/svg/",
	"libs/flake/",
	"libs/widgets/",
	"pics/",
	"krita/data/aboutdata/",
	"krita/data/shaders/",
	"krita/data/cursors/",
	]
	
# Loop through each location
for location in iconLocations: 

    fullLocation = kritaSourceCodeLocation + location # append git directory with icon path inside 

    for filename in os.listdir(fullLocation):
        if filename.endswith(".svg") or filename.endswith(".png") or filename.endswith(".svgz"):
            originalFile = os.path.join(fullLocation, filename)
            copiedFile = os.path.join(iconDictionaryLocation, filename)
            
            copyfile(originalFile, copiedFile ) # actually do the file copying
            
        else:
            continue
            
            
# we gathered all the images into one folder. Now we need to make a JSON file to use for our website to load
dataToExport = []
idCounter = 0 # we have various versions of icons like PNG and SVG, dark and light, so good to keep a unique ID for each element for selections
for filename in os.listdir(iconDictionaryLocation):
    #print(filename)

    item = {
      "id": 0,
      "name": "",
      "format": "",
      "isThemed": False,
      "theme": ""
    }
    
    item["id"] = idCounter;
    idCounter = idCounter + 1
    
    
    # when you use icons, you don't have to put "light" or "dark". Krita takes care of that. 
    # For documentation purposes strip that out
    filenameToShow = os.path.splitext(filename)[0]
    
    if filenameToShow.startswith( 'dark_' ) or filenameToShow.startswith( 'light_' ):
        item["isThemed"] = True
        filenameToShow = filenameToShow.replace("light_", "")
        filenameToShow = filenameToShow.replace("dark_", "")
    
    item["name"] = filenameToShow
    
    # add data for file extension
    fileExtension = os.path.splitext(filename)[1]  
    item["format"] = fileExtension[1::] # this is a fancy way of removing the "." symbol at the beginning (substring)
        
    
    if "dark" in filename:
        item["theme"] = "dark"
    elif "light" in filename:
        item["theme"] = "light"
    else:
        item["theme"] = "none"

    
    dataToExport.append(item)

#print( json.dumps(dataToExport) )

# save the file off to the hard drive in the main directory for now. 
with open(kritaScriptingRepoLocation + "icon-dictionary.json", 'w') as outfile:
    json.dump(dataToExport, outfile, skipkeys=False, ensure_ascii=True, check_circular=True, allow_nan=True, cls=None, indent=4)


print('Script is done running. Check the root directory of this project and hopefully the JSON file is generated and the image files are all extracted from the Krita source code')

